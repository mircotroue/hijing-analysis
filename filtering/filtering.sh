#!/bin/bash

#hijingfile=$hijingpath/$i/HIJING_LBF_test_small.out
hijingfile=$i/HIJING_LBF_test_small.out

# create temporary dirs for storing events
tmpdir=$(mktemp -d)
echo "temporary dir" $tmpdir "created."

array=( $(grep -n BEGINNINGOFEVENT $hijingfile | cut -f1 -d:) )
array+=( $(wc -l $hijingfile | awk '{print $1}') )

# split into single events and save to dat files
for j in {0..$(($(grep BEGINNINGOFEVENT $hijingfile | wc -l)-1))}; do
    sed -n "$(($array[j+1]+2)),$(($array[j+2]-1))p" $hijingfile | awk '{ if ($3 == 0) { print $2, $5, $6, $7, $8} }' $hijingfile >> $tmpdir/$j.dat
done

# convert into ttrees
root -l -b -q filtering/ascii2ttree.C\(\"$tmpdir\"\,\"$i\"\)

# clean up temporary folders
rm -r $tmpdir
echo "Cleaning up" $tmpdir
