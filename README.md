# hijing-analysis

HIJING (Heavy Ion Jet INteraction Generator) is widely used Monte Carlo generator in high-energy proton-proton, proton-nucleus and nucleus-nucleus collisions. The physics incorporated in this model is based on QCD-inspired models for jets production, and includes multiple minijet production, soft excitation, nuclear shadowing of parton distribution functions and jet interaction in dense matter.

In this project, you are challanged to use combined Linux, Bash and ROOT functionalities covered in the lecture, in order to fully automate the data analysis over one typical HIJING dataset, generated in 10 separate jobs on a local batch farm. 

## Setup
```bash
tar -xf hijing-data.tgz -C /path/to/extract
```

## Usage
```bash
source run.sh /path/to/extract/hijing-data
```
Write permission for the data directory is necessary to create single event root files. Root has to be available in the current session.
