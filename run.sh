#!/bin/bash

hijingpath=$1

# clean up old data
rm trending/averaged.txt
rm analysis/root-files.txt

# loop over all folders in hijingpath and start parallel filtering/trending for each folder
for i in $hijingpath/*; do
    (
        source filtering/filtering.sh
        root -l -b -q trending/countparticles.C\($(basename $i)\,\"$i/HIJING_LBF_test_small.root\"\)
    ) &
done
wait

# trend plotting
root -l -b -q trending/plotting.C

# final analysis
root -l -b -q analysis/analysis.C
